package com.example.billy.tvbgoner2.Data;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Data {
    public static ArrayList<PowerCode> NACodes;
    public static ArrayList<PowerCode> EUCodes;
    public static Context context;
    public Data(){
    }
    public static void setData(){
        NACodes = new ArrayList<>();
        EUCodes = new ArrayList<>();
        NACodes = addData(loadData("NATiming.txt"));
        EUCodes = addData(loadData("EUTiming.txt"));
    }

    private static String loadData(String s) {
        AssetManager am = context.getAssets();
        InputStream is = null;
        try {
            is = am.open(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line,fullData = "";
        try {
            while ((line = reader.readLine()) != null)
                fullData += line;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return fullData;
    }

    private static ArrayList<PowerCode> addData(String data) {
        ArrayList<PowerCode> temp = new ArrayList<>();
        String line = data;
        int counter = 0;
        while(!line.isEmpty()) {
            counter++;
            int lBI = line.indexOf('{');
            int rBI = line.indexOf('}');
            String sub = line.substring(lBI+1,rBI);
            line = line.substring(rBI+1,line.length());
            PowerCode c = new PowerCode();
            int cI = sub.indexOf(':');
            c.setFrequency(Integer.parseInt(sub.substring(0,cI)));
            c.add(Integer.parseInt(sub.substring(cI+1,sub.indexOf(','))));
            int lI = sub.indexOf(',')+1;
            int rI = sub.indexOf(',',lI+1);
            while(lI < sub.length()){
                c.add(Integer.parseInt(sub.substring(lI,rI)));
                lI = rI+1;
                rI = sub.indexOf(',',lI+1);
                if(rI == -1)
                    rI = sub.length();
            }
            temp.add(c);
        }
        return temp;
    }
}
