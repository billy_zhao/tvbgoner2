package com.example.billy.tvbgoner2.Logic;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.hardware.ConsumerIrManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.example.billy.tvbgoner2.Data.Data;
import com.example.billy.tvbgoner2.Data.PowerCode;
import com.example.billy.tvbgoner2.MainActivity;

import java.util.ArrayList;
import android.content.Context;
/**
 * Created by billy on 6/22/2015.
 */
public class IRSignaler extends AsyncTask<Void, Void, Void> {
    ConsumerIrManager mCIR;
    ArrayList<PowerCode> pcodes;
    Context context;
    int lastIdx;
    public ProgressDialog dialog;
    public int progress;
    int VERSION_MR;
    public IRSignaler(Context c,ConsumerIrManager i, ArrayList<PowerCode> p) {
        mCIR = i;
        pcodes = p;
        context = c;
    }

    @Override
    protected void onPreExecute() {
        progress = 0;
        lastIdx = Build.VERSION.RELEASE.lastIndexOf(".");
        VERSION_MR = Integer.valueOf(Build.VERSION.RELEASE.substring(lastIdx + 1));
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setMessage("Turning off TVs");
        dialog.setProgressNumberFormat(null);
        dialog.setCancelable(false);
        dialog.setProgress(0);
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        Toast.makeText(context,
                                "Cancel clicked!", Toast.LENGTH_SHORT).show();
                        cancel(true);
                        return;
                    }
                });
        dialog.show();

    }

    @Override
    protected Void doInBackground(Void... params) {
            for (PowerCode p : pcodes) {
                if(!isCancelled()) {
                progress++;
                int[] x = p.getPattern();
                int[] z = new int[x.length];
                for (int i = 0; i < x.length; i++) {
                    if (p.getFrequency() == 0) {
                        z[i] = (int) (38500 / 1000000.0 * x[i]);
                    } else {
                        z[i] = (int) (p.getFrequency() / 1000000.0 * x[i]);
                    }
                }

                    if (VERSION_MR < 3) {
                        // Before version of Android 4.4.2
                        if (p.getFrequency() == 0) {
                            mCIR.transmit(38500, z);
                        } else {
                            mCIR.transmit(p.getFrequency(), z);
                        }
                    } else {
                        // Later version of Android 4.4.3
                        mCIR.transmit(p.getFrequency(), x);
                    }
                }else{

                }
                publishProgress();
            }
        return null;
        }

    @Override
    protected void onPostExecute(Void aVoid) {
        Log.v("codes", "All codes successfully executed");
        if(isCancelled()){
            dialog.dismiss();
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        int percent = Math.round(100*progress/pcodes.size());
        dialog.setProgress(percent);
    }
}

