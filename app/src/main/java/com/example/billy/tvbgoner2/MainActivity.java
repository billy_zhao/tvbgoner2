package com.example.billy.tvbgoner2;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.ConsumerIrManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.billy.tvbgoner2.Data.Data;
import com.example.billy.tvbgoner2.Logic.IRSignaler;


public class MainActivity extends Activity {
    ConsumerIrManager mCIR;
    Handler _progressHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Data.context = getApplicationContext();
        Data.setData();
        setContentView(R.layout.activity_main);
        ImageButton button = (ImageButton) this.findViewById(R.id.imageView3);
        button.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
        button = (ImageButton) this.findViewById(R.id.imageView4);
        button.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent getNameScreenIntent = new Intent(this,SettingsScreen.class);
            startActivity(getNameScreenIntent);
            return true;
        }else if(id == R.id.exit_the_app) {
            DialogFragment fragment = new ExitDialogFragment();
            fragment.show(getFragmentManager(),"exitDialog");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void powerButtonPressed(View v){
        IRSignaler ir;
        mCIR = (ConsumerIrManager)getSystemService(CONSUMER_IR_SERVICE);
        switch((String)(v.getTag())){
            case "NA":
                ir = new IRSignaler(this,mCIR,Data.NACodes);
                ir.execute();
                break;
            case "EU":
                ir = new IRSignaler(this,mCIR,Data.EUCodes);
                ir.execute();
                break;
            default:
                break;
        }
    }
}
