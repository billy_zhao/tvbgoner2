package com.example.billy.tvbgoner2.Data;

import java.util.ArrayList;

public class PowerCode {
    private int frequency;
    private ArrayList<Integer> pattern = new ArrayList<>();
    public PowerCode(){
    }
    public PowerCode(int freq, ArrayList<Integer> patt){
        frequency = freq;
        pattern = patt;
    }
    public int getFrequency(){
        return frequency;
    }
    public int[] getPattern(){
        int[] ret = new int[pattern.size()];
        for (int i=0; i < ret.length; i++)
            ret[i] = pattern.get(i).intValue();
        return ret;
    }
    public void setFrequency(int x){
        frequency = x;
    }
    public void add(int x){
        pattern.add(x);
    }
}
